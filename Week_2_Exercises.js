// 1. Create an object representation of yourself
// Should include:
// - firstName
// - lastName
// - 'favorite food'
// - bestFriend (object with the same 3 properties as above)

let me = {
    firstName: 'Alex',
    lastName: 'Palmieri',
    'favorite food': 'Chipotle',
    bestFriend: {firstName: 'Chandler', lastName: 'Bing', 'favorite food': 'pizza'}
};

console.log('Best Friends FirstName: '+me.bestFriend.firstName);
console.log('My Favorite Food: '+me["favorite food"]);


// 3. Create an array to represent this tic-tac-toe board
// -O-
// -XO
// X-X
let gameBoard = [];
let row1 = ['-','O','-'];
let row2 = ['-','X','O'];
let row3 = ['X','-','X'];

gameBoard.push(row1);
gameBoard.push(row2);
gameBoard.push(row3);

console.log(gameBoard[0]);
console.log(gameBoard[1]);
console.log(gameBoard[2]);

console.log('--------------------------------');

// 4. After the array is created, 'O' claims the top right square.
// Update that value.

gameBoard[0][2] = 'O';

// 5. Log the grid to the console.

console.log(gameBoard[0]);
console.log(gameBoard[1]);
console.log(gameBoard[2]);

// 6. You are given an email as string myEmail, make sure it is in correct email format.
// Should be 1 or more characters, then @ sign, then 1 or more characters, then dot, then one or more characters - no whitespace
// i.e. foo@bar.baz
// Hints:
// - Use rubular to check a few emails: https://rubular.com/
// - Use regexp test method https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp/test
let myExpression = /[\w\.-]+@[\w\.-]+\.\w{2,4}/;

let myTestEmail = [
  'foo@bar.biz','email@email.com','@email.com','123@emailcom',
  '123@email.com','test.email@myTestEmail.com','test-hyph@email.com',
  'test@email.edu.eu'
];

myTestEmail.forEach((item) => {
  try {
      console.log(item.match(myExpression).input);
  }catch(err) {
    console.log(`Invalid Email --> ${item}`);
  }
});

// 7. You are given an assignmentDate as a string in the format "month/day/year"
// i.e. '1/21/2019' - but this could be any date.
// Convert this string to a Date
const assignmentDate = '1/21/2019';

let month = assignmentDate.slice(0,assignmentDate.indexOf('/'));
let day = assignmentDate.slice(assignmentDate.indexOf('/')+1,assignmentDate.lastIndexOf('/'));
let year = assignmentDate.slice(assignmentDate.lastIndexOf('/')+1,assignmentDate.length);

let assignmentDateDA = new Date(parseInt(year),parseInt(month)-1,parseInt(day))

console.log(`--> Assignment Date String: ${assignmentDate}
--> Assignment Date Date: ${assignmentDateDA}`);

// 8. Create a new Date instance to represent the dueDate.
// This will be exactly 7 days after the assignment date.

let dueDate = addDays(assignmentDateDA,7);

console.log(`--> Due Date String: ${dueDate}`);

function addDays(startDate, numOfDays) {
  tmpDate = new Date(startDate);
  tmpDate = tmpDate.setDate(tmpDate.getDate()+numOfDays);
  return new Date(tmpDate);
};

// 9. Use dueDate values to create an HTML time tag in format
// <time datetime="YYYY-MM-DD">Month day, year</time>
// I have provided a months array to help
const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
];

let timeTag = `<p><time datetime=${dueDate.getFullYear()+'-'+months[dueDate.getMonth()]+'-'+dueDate.getDay().toString().padStart(2,'0')}>Due Date</time></p>`;
document.getElementById('timeTag').innerHTML = timeTag;

// 10. log this value using console.log
console.log(document.getElementsByTagName('time')[0]);
